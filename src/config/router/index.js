import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {Home, Order, NewsDetail, OrderDetail} from '../../containers/pages';
import {View} from 'react-native';

const HomePage = ({navigation}) => {
  return (
    <Home
      onPress={() => navigation.navigate('NewsDetail')}
      onPress1={() => alert('Home')}
      onPress2={() => navigation.navigate('Order')}
      onPress3={() => alert('Help')}
      onPress4={() => alert('Inbox')}
      onPress5={() => alert('Account')}
    />
  );
};

const NewsDetailPage = ({navigation}) => {
  return <NewsDetail />;
};

const OrderPage = ({navigation}) => {
  return <Order onPress={() => navigation.navigate('OrderDetail')} />;
};

const OrderDetailPage = ({navigation}) => {
  return <OrderDetail />;
};

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Home" component={HomePage} />
        <Stack.Screen name="NewsDetail" component={NewsDetailPage} />
        <Stack.Screen name="Order" component={OrderPage} />
        <Stack.Screen name="OrderDetail" component={OrderDetailPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const Router = () => {
  return <HomeStack />;
};

export default Router;
