import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const Button = (props) => {
  return (
    <TouchableOpacity style={styles.goNewsButton} onPress={props.onPress}>
      <Text style={styles.goButtonText}>{props.GoButton}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  goNewsButton: {
    backgroundColor: '#3a9f40',
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 11,
    alignSelf: 'flex-end',
  },
  goButtonText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
});
