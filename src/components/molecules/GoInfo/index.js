import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

// ATOM
import Button from '../../atoms/Button';

const GoInfo = (props) => {
  return (
    <View>
      <View>
        <Image source={props.GojekIcon2} style={{width: 60, height: 15}} />
      </View>
      <View style={{paddingVertical: 16}}>
        <Text style={{fontSize: 17, fontWeight: 'bold', color: '#1C1C1C'}}>
          Complete your Profile
        </Text>
      </View>
      <View style={{flexDirection: 'row', paddingBottom: 16}}>
        <View style={{marginRight: 16}}>
          <Image source={props.GoInfoImg} style={{height: 60, width: 120}} />
        </View>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#4A4A4A',
            }}>
            {props.GoInfoTitle}
          </Text>
          <Text style={{fontSize: 15, color: '#4A4A4A', width: '70%'}}>
            {props.GoInfoDesc}
          </Text>
        </View>
      </View>
      <Button GoButton="CONNECT" />
      <View
        style={{
          width: 360,
          borderBottomColor: '#E8E9ED',
          borderBottomWidth: 2,
          paddingVertical: 6,
        }}></View>
    </View>
  );
};

export default GoInfo;
