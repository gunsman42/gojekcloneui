import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

// ATOM
import Button from '../../atoms/Button';

const GoFoodBanner = (props) => {
  return (
    <View>
      <View style={{position: 'relative'}}>
        <Image source={props.Image} style={styles.goNewsBannerIcon} />
        <View style={styles.goNewsFadeBackground}></View>
        <View style={styles.goNewsGojekLogo}>
          <Image
            source={props.Logo}
            style={{
              width: undefined,
              height: undefined,
              resizeMode: 'contain',
              flex: 1,
            }}
          />
        </View>
        <View
          style={{
            position: 'absolute',
            left: 0,
            bottom: 0,
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 16,
          }}>
          <View>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: 'white',
                marginBottom: 10,
              }}>
              Free GO-FOOD Voucher
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: 'bold',
                color: 'white',
                paddingBottom: 10,
              }}>
              Quick, before they runout!
            </Text>
          </View>
          <View style={{flex: 1, paddingLeft: 16}}>
            <Button GoButton="GET VOUCHER" />
          </View>
        </View>
      </View>
    </View>
  );
};

export default GoFoodBanner;

const styles = StyleSheet.create({
  goNewsBannerIcon: {
    height: 175,
    width: 362,
    borderRadius: 6,
  },
  goNewsFadeBackground: {
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
    opacity: 0.25,
    position: 'absolute',
    top: 0,
    left: 0,
    borderRadius: 6,
  },
  goNewsGojekLogo: {
    height: 15,
    width: 58,
    position: 'absolute',
    marginHorizontal: 20,
    marginTop: 20,
  },
  goNewsTextWrapper: {
    paddingHorizontal: 4,
    paddingVertical: 16,
    borderBottomColor: '#E8E9ED',
    borderBottomWidth: 2,
  },
  goNewsTextDesc: {
    fontSize: 14,
    fontWeight: '600',
    color: '#7A7A7A',
    paddingTop: 4,
    marginBottom: 11,
  },
});
