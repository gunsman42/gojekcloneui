import React from 'react';
import {StyleSheet, Text, View, Image, TextInput} from 'react-native';

//IMPORT ICON GENERAL
import SearchIcon from '../../../assets/icon/search.png';
import PromoIcon from '../../../assets/icon/promo.png';

const SearchFeature = (props) => {
  return (
    <View style={styles.headBarWrapper}>
      <View style={styles.searchBarContainer}>
        <TextInput
          placeholder="What do you want to eat?"
          style={styles.searchBarInput}
        />
        <Image source={SearchIcon} style={styles.SearchIconImg} />
      </View>
      <View style={styles.promoIconImg}>
        <Image source={PromoIcon} />
      </View>
    </View>
  );
};

export default SearchFeature;

const styles = StyleSheet.create({
  headBarWrapper: {
    marginHorizontal: 17,
    marginTop: 15,
    flexDirection: 'row',
  },
  searchBarContainer: {
    position: 'relative',
    flex: 1,
  },
  searchBarInput: {
    borderWidth: 1,
    borderColor: '#E8E8E8',
    borderRadius: 25,
    height: 40,
    fontSize: 13,
    paddingLeft: 45,
    paddingRight: 20,
    backgroundColor: 'white',
    marginRight: 18,
  },
  SearchIconImg: {
    position: 'absolute',
    top: 6,
    left: 12,
  },
  promoIconImg: {
    width: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
