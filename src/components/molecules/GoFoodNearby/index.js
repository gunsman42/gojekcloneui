import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

const GoFoodNearby = (props) => {
  return (
    <View>
      <View>
        <Image source={props.IconImg} style={{width: 64, height: 15}} />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingVertical: 10,
        }}>
        <View>
          <Text
            style={{
              fontSize: 17,
              fontWeight: 'bold',
              color: '#4A4A4A',
            }}>
            Nearby Restaurants
          </Text>
        </View>
        <TouchableOpacity>
          <Text
            style={{
              fontSize: 17,
              color: '#3a9f40',
              fontWeight: 'bold',
            }}>
            See All
          </Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{flexDirection: 'row'}}>
        <View style={{paddingRight: 16}}>
          <View style={{height: 150, width: 150}}>
            <Image
              source={props.ImageMenu1}
              style={{
                width: undefined,
                height: undefined,
                resizeMode: 'cover',
                flex: 1,
                borderRadius: 8,
              }}
            />
          </View>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#1c1c1c',
              paddingVertical: 12,
            }}>
            {props.MenuName1}
          </Text>
        </View>
        <View style={{paddingRight: 16}}>
          <View style={{height: 150, width: 150}}>
            <Image
              source={props.ImageMenu2}
              style={{
                width: undefined,
                height: undefined,
                resizeMode: 'cover',
                flex: 1,
                borderRadius: 8,
              }}
            />
          </View>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#1c1c1c',
              paddingVertical: 12,
            }}>
            {props.MenuName2}
          </Text>
        </View>
        <View style={{paddingRight: 16}}>
          <View style={{height: 150, width: 150}}>
            <Image
              source={props.ImageMenu3}
              style={{
                width: undefined,
                height: undefined,
                resizeMode: 'cover',
                flex: 1,
                borderRadius: 8,
              }}
            />
          </View>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#1c1c1c',
              paddingVertical: 12,
            }}>
            {props.MenuName3}
          </Text>
        </View>
        <View style={{paddingRight: 16}}>
          <View style={{height: 150, width: 150}}>
            <Image
              source={props.ImageMenu4}
              style={{
                width: undefined,
                height: undefined,
                resizeMode: 'cover',
                flex: 1,
                borderRadius: 8,
              }}
            />
          </View>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#1c1c1c',
              paddingVertical: 12,
            }}>
            {props.MenuName4}
          </Text>
        </View>
        <View style={{paddingRight: 16}}>
          <View style={{height: 150, width: 150}}>
            <Image
              source={props.ImageMenu5}
              style={{
                width: undefined,
                height: undefined,
                resizeMode: 'cover',
                flex: 1,
                borderRadius: 8,
              }}
            />
          </View>
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#1c1c1c',
              paddingVertical: 12,
            }}>
            {props.MenuName5}
          </Text>
        </View>
      </ScrollView>
      <View
        style={{
          width: 360,
          borderBottomColor: '#E8E9ED',
          borderBottomWidth: 2,
          paddingVertical: 6,
        }}></View>
    </View>
  );
};

export default GoFoodNearby;

const styles = StyleSheet.create({});
