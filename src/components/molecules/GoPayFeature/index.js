import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const GoPayFeature = (props) => {
  return (
    <View style={styles.goPayBodyContainer}>
      <Image source={props.GoPayFeatureIcon} style={styles.GoPayBodyIcon} />
      <Text style={styles.GoPayBodyText}>{props.title}</Text>
    </View>
  );
};

export default GoPayFeature;

const styles = StyleSheet.create({
  goPayBodyContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 10,
  },
  GoPayBodyIcon: {
    width: 36,
    height: 36,
  },
  GoPayBodyText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 8,
  },
});
