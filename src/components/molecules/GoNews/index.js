import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const GoNews = (props) => {
  return (
    <View style={styles.goNewsWrapper}>
      <View style={{position: 'relative'}}>
        <Image source={props.GoNewsImg} style={styles.goNewsBannerIcon} />
        <View style={styles.goNewsFadeBackground}></View>
        <View style={styles.goNewsGojekLogo}>
          <Image
            source={props.GoJekIcon}
            style={{
              width: undefined,
              height: undefined,
              resizeMode: 'contain',
              flex: 1,
            }}
          />
        </View>
      </View>
      <View style={styles.goNewsTextWrapper}>
        <Text style={styles.goNewsTextTitle}>{props.GoNewsTitle}</Text>
        <Text style={styles.goNewsTextDesc}>{props.goNewsTextDesc}</Text>
        <TouchableOpacity style={styles.goNewsButton} onPress={props.onPress}>
          <Text style={styles.goButtonText}>READ</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default GoNews;

const styles = StyleSheet.create({
  goNewsWrapper: {
    padding: 12,
  },
  goNewsBannerIcon: {
    height: 175,
    width: 362,
    borderRadius: 6,
  },
  goNewsFadeBackground: {
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
    opacity: 0.25,
    position: 'absolute',
    top: 0,
    left: 0,
    borderRadius: 6,
  },
  goNewsGojekLogo: {
    height: 15,
    width: 58,
    position: 'absolute',
    marginHorizontal: 20,
    marginTop: 20,
  },
  goNewsTextWrapper: {
    paddingHorizontal: 4,
    paddingVertical: 16,
    borderBottomColor: '#E8E9ED',
    borderBottomWidth: 2,
  },
  goNewsTextTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1C1C1C',
  },
  goNewsTextDesc: {
    fontSize: 14,
    fontWeight: '600',
    color: '#7A7A7A',
    paddingTop: 4,
    marginBottom: 11,
  },
  goNewsButton: {
    backgroundColor: '#3a9f40',
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 11,
    alignSelf: 'flex-end',
  },
  goButtonText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
});
