import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const GoPayFeatureMenu = (props) => {
  return (
    <View style={styles.mainFeatureBoxWrapper}>
      <View style={styles.mainFeatureBox}>
        <Image
          source={props.GoPayFeatureMenuImg}
          style={styles.mainFeatureIcon}
        />
      </View>
      <Text style={styles.mainFeatureText}>{props.GoPayFeatureMenuText}</Text>
    </View>
  );
};

export default GoPayFeatureMenu;

const styles = StyleSheet.create({
  mainFeatureBoxWrapper: {
    width: '25%',
    alignItems: 'center',
  },
  mainFeatureBox: {
    width: 58,
    height: 58,
    borderWidth: 1,
    borderColor: '#EFEFEF',
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainFeatureIcon: {
    width: 54,
    height: 54,
  },
  mainFeatureText: {
    textAlign: 'center',
    fontSize: 11,
    paddingTop: 6,
    fontWeight: 'bold',
    color: 'grey',
  },
});
