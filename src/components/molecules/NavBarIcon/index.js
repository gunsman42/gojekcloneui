import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const NavBarIcon = (props) => {
  return (
    <TouchableOpacity style={styles.underBarItem} onPress={props.onPress}>
      <Image source={props.icon} style={styles.underBarItemImgContainer} />
      <Text
        style={{
          fontSize: 10,
          color: props.active ? '#43AB4A' : '#545454',
          marginTop: 4,
        }}>
        {props.iconText}
      </Text>
    </TouchableOpacity>
  );
};

export default NavBarIcon;

const styles = StyleSheet.create({
  underBarItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  underBarItemImgContainer: {
    width: 26,
    height: 26,
  },
});
