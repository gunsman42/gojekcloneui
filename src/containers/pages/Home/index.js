import React, {Component} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';

// Molecules
import SearchFeature from '../../../components/molecules/SearchFeature';

//Ogranisms
import NavBar from '../../organisms/NavBar';
import HomeMainFeature from '../../organisms/HomeMainFeature';
import HomeNewsSection from '../../organisms/HomeNewsSection';
import GoPaySection from '../../organisms/GoPaySection';

export class Home extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.mainMenu}>
          <SearchFeature />
          {/* GoPay */}
          <GoPaySection />
          {/* Main Feature */}
          <HomeMainFeature />
          <View style={styles.appSeperater}></View>
          {/* News Section */}
          <HomeNewsSection onPress={this.props.onPress} />
        </ScrollView>
        {/* Bottom Navigation Bar */}
        <NavBar
          onPress1={this.props.onPress1}
          onPress2={this.props.onPress2}
          onPress3={this.props.onPress3}
          onPress4={this.props.onPress4}
          onPress5={this.props.onPress5}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainMenu: {
    flex: 1,
    backgroundColor: 'white',
  },
  appSeperater: {
    height: 17,
    backgroundColor: '#F2F2F4',
  },
});

export default Home;
