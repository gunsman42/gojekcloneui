import React, {Component} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import {Text} from 'react-native';

import Button from '../../../components/atoms/Button';
import NavBar from '../../../containers/organisms/NavBar';

export class Orders extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <Text>Order Page</Text>
          <Button GoButton="ORDER DETAIL" onPress={this.props.onPress} />
        </View>
        <NavBar />
        {/* <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.mainMenu}>
          <SearchFeature />

          <GoPaySection />

          <HomeMainFeature />
          <View style={styles.appSeperater}></View>

          <HomeNewsSection />
        </ScrollView>

        <NavBar /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainMenu: {
    flex: 1,
    backgroundColor: 'white',
  },
  appSeperater: {
    height: 17,
    backgroundColor: '#F2F2F4',
  },
});

export default Orders;
