import Home from './Home';
import NewsDetail from './NewsDetail';
import Order from './Orders';
import OrderDetail from './OrderDetail';

export {Home, NewsDetail, Order, OrderDetail};
