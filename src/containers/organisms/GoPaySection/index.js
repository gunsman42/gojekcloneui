import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Text, View, Image} from 'react-native';

import GoPayFeature from '../../../components/molecules/GoPayFeature';

// IMPORT ICON GOPAY
import GoPayPayIcon from '../../../assets/icon/pay.png';
import GoPayNearbyIcon from '../../../assets/icon/nearby.png';
import GoPayTopupIcon from '../../../assets/icon/topup.png';
import GoPayMoreIcon from '../../../assets/icon/more.png';
import GoPayIcon from '../../../assets/icon/gopay.png';

export class GoPaySection extends Component {
  render() {
    return (
      <View style={styles.goPayWrapper}>
        <View style={styles.goPayHeader}>
          <Image source={GoPayIcon} />
          <Text style={styles.GoPayText}>Rp 53.000</Text>
        </View>
        <View style={styles.goPayBodyWrapper}>
          <GoPayFeature title="Pay" GoPayFeatureIcon={GoPayPayIcon} />
          <GoPayFeature title="Nearby" GoPayFeatureIcon={GoPayNearbyIcon} />
          <GoPayFeature title="Top Up" GoPayFeatureIcon={GoPayTopupIcon} />
          <GoPayFeature title="More" GoPayFeatureIcon={GoPayMoreIcon} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  goPayWrapper: {
    marginHorizontal: 20,
    marginTop: 14,
  },
  goPayHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#2C5FB8',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    padding: 17,
  },
  GoPayText: {
    fontSize: 17,
    fontWeight: '700',
    color: 'white',
    letterSpacing: 1,
  },
  goPayBodyWrapper: {
    flexDirection: 'row',
    backgroundColor: '#2F65BD',
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },
});

export default GoPaySection;
