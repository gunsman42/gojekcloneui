import React, {Component} from 'react';
import {Text, View} from 'react-native';

//IMPORT GO NEWS IMAGE
import GoJekIcon from '../../../assets/logo/white.png';
import GojekIcon2 from '../../../assets/logo/gojek.png';
import GoFoodIcon from '../../../assets/logo/go-food.png';
import GoNewsFoodBanner from '../../../assets/dummy/food-banner.jpg';
import GoNewsSoccerBanner from '../../../assets/dummy/sepak-bola.jpg';
import GoNewsFacebook from '../../../assets/dummy/facebook-connect.png';

import GoNews from '../../../components/molecules/GoNews';
import GoInfo from '../../../components/molecules/GoInfo';
import GoFoodBanner from '../../../components/molecules/GoFoodBanner';

import NearbySection from '../NearbySection';
import {StyleSheet} from 'react-native';

export class HomeNewsSection extends Component {
  render() {
    return (
      <View style={styles.goNewsWrapper}>
        {/* GO-NEWS */}
        <GoNews
          GoNewsImg={GoNewsSoccerBanner}
          GoJekIcon={GoJekIcon}
          GoNewsTitle="GO-NEWS"
          goNewsTextDesc="MANTAP! Serangan balasan, menggocek bola tetapi terpeleset di lapangan"
          onPress={this.props.onPress}
        />
        {/* INTERNAL INFORMATION */}
        <View style={styles.goNewsWrapper}>
          <GoInfo
            GojekIcon2={GojekIcon2}
            GoInfoImg={GoNewsFacebook}
            GoInfoTitle="Connect with Facebook"
            GoInfoDesc="Login faster without verification code"
          />
        </View>
        {/* News Food Section */}
        <View style={styles.goNewsWrapper}>
          <GoFoodBanner Image={GoNewsFoodBanner} Logo={GoJekIcon} />
          <View
            style={{
              width: 360,
              borderBottomColor: '#E8E9ED',
              borderBottomWidth: 2,
              paddingVertical: 6,
            }}></View>
        </View>
        <NearbySection />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  goNewsWrapper: {
    padding: 12,
  },
});

export default HomeNewsSection;
