import React, {Component} from 'react';
import {Text, View} from 'react-native';

import GoPayFeatureMenu from '../../../components/molecules/GoPayFeatureMenu';

//IMPORT ICON MAIN FEATURE
import GoRide from '../../../assets/icon/go-ride.png';
import GoCar from '../../../assets/icon/go-car.png';
import GoBlueBird from '../../../assets/icon/go-bluebird.png';
import GoSend from '../../../assets/icon/go-send.png';
import GoDeals from '../../../assets/icon/go-deals.png';
import GoPulsa from '../../../assets/icon/go-pulsa.png';
import GoFood from '../../../assets/icon/go-food.png';
import GoMore from '../../../assets/icon/go-more.png';
import {StyleSheet} from 'react-native';

export class HomeMainFeature extends Component {
  render() {
    return (
      <View style={styles.mainFeatureWrapper}>
        <View style={styles.wrapMenuFeature}>
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoRide}
            GoPayFeatureMenuText="GO-RIDE"
          />
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoCar}
            GoPayFeatureMenuText="GO-CAR"
          />
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoBlueBird}
            GoPayFeatureMenuText="GO-BLUEBIRD"
          />
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoSend}
            GoPayFeatureMenuText="GO-SEND"
          />
        </View>
        <View style={styles.wrapMenuFeature}>
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoDeals}
            GoPayFeatureMenuText="GO-DEALS"
          />
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoPulsa}
            GoPayFeatureMenuText="GO-PULSA"
          />
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoFood}
            GoPayFeatureMenuText="GO-FOOD"
          />
          <GoPayFeatureMenu
            GoPayFeatureMenuImg={GoMore}
            GoPayFeatureMenuText="MORE"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainFeatureWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 0,
    marginTop: 14,
  },
  wrapMenuFeature: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 14,
  },
});

export default HomeMainFeature;
