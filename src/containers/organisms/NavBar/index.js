import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

import NavBarIcon from '../../../components/molecules/NavBarIcon';

//IMPORT ICON NON ACTIVE
import HomeIcon from '../../../assets/icon/home.png';
import OrderIcon from '../../../assets/icon/order.png';
import HelpIcon from '../../../assets/icon/help.png';
import InboxIcon from '../../../assets/icon/inbox.png';
import AccountIcon from '../../../assets/icon/account.png';

//IMPORT ICON ACTIVE
import HomeIconActive from '../../../assets/icon/home-active.png';
import OrderIconActive from '../../../assets/icon/order-active.png';
import HelpIconActive from '../../../assets/icon/help-active.png';
import InboxIconActive from '../../../assets/icon/inbox-active.png';
import AccountIconActive from '../../../assets/icon/account-active.png';

export class Navbar extends Component {
  render() {
    return (
      <View style={styles.underBarContainer}>
        <NavBarIcon
          icon={HomeIconActive}
          iconText="Home"
          active
          onPress={this.props.onPress1}
        />
        <NavBarIcon
          icon={OrderIcon}
          iconText="Orders"
          onPress={this.props.onPress2}
        />
        <NavBarIcon
          icon={HelpIcon}
          iconText="Help"
          onPress={this.props.onPress3}
        />
        <NavBarIcon
          icon={InboxIcon}
          iconText="Inbox"
          onPress={this.props.onPress4}
        />
        <NavBarIcon
          icon={AccountIcon}
          iconText="Account"
          onPress={this.props.onPress5}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  underBarContainer: {
    height: 54,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
});

export default Navbar;
