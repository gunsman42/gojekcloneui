import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

import GoFoodNearby from '../../../components/molecules/GoFoodNearby';

import GoNewsFoodMenu1 from '../../../assets/dummy/go-food-kfc.jpg';
import GoNewsFoodMenu2 from '../../../assets/dummy/go-food-banka.jpg';
import GoNewsFoodMenu3 from '../../../assets/dummy/go-food-gm.jpg';
import GoNewsFoodMenu4 from '../../../assets/dummy/go-food-orins.jpg';
import GoNewsFoodMenu5 from '../../../assets/dummy/go-food-pak-boss.jpg';
import GoFoodIcon from '../../../assets/logo/go-food.png';

export class NearbySection extends Component {
  render() {
    return (
      <View style={styles.goNewsWrapper}>
        <GoFoodNearby
          IconImg={GoFoodIcon}
          ImageMenu1={GoNewsFoodMenu1}
          ImageMenu2={GoNewsFoodMenu2}
          ImageMenu3={GoNewsFoodMenu3}
          ImageMenu4={GoNewsFoodMenu4}
          ImageMenu5={GoNewsFoodMenu5}
          MenuName1="Wingstop"
          MenuName2="Martabak Fatma"
          MenuName3="Bakmie Gunsman"
          MenuName4="Martabak Meledak"
          MenuName5="Ayam Geprek BOSS"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  goNewsWrapper: {
    padding: 12,
  },
});

export default NearbySection;
