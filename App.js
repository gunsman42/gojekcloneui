import React from 'react';
import {View, StyleSheet} from 'react-native';

import Router from './src/config/router';

import BottomNavigator from './src/containers/organisms/BottomNavigator';

function App() {
  return <Router />;
  // <BottomNavigator />;
}

const styles = StyleSheet.create({});

export default App;
